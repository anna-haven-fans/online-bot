#!/usr/bin/env python3

import time
import dhooks
import logging
import requests
import threading
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
log = logging.getLogger(__name__)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
log.setLevel(logging.INFO)
log.addHandler(ch)

annahaven_url = 'https://chaturbate.com/annahaven/'
webhook_url = 'https://discordapp.com/api/webhooks/WEBHOOK_URL'

last_online = 0
stop = threading.Event()
hook = dhooks.Webhook(webhook_url)
options = Options()
options.add_argument('--headless')


def online():
    global last_online

    while not stop.is_set():
        try:
            log.info('Checking...')

            page = requests.get(annahaven_url)

            if page.status_code == 200:
                d = webdriver.Firefox(options=options)
                d.implicitly_wait(15)
                d.get(annahaven_url)
                d.find_element_by_link_text('I AGREE').click()
                page = d.find_element_by_id("main")

                # log.info(page.text)
                if 'offline' not in page.text:
                    if last_online == 0:
                        msg = 'Anna is now online'
                        log.info(msg)
                        hook.send(msg)
                    last_online = time.time()
                elif last_online > 0 and last_online < time.time() - 120:
                    last_online = 0
                    msg = 'Anna went offline'
                    log.info(msg)
                    hook.send(msg)
                else:
                    msg = 'Anna still offline'
                    log.info(msg)
            d.quit()
            time.sleep(45)
        except (KeyboardInterrupt, SystemExit):
            stop.set()
            pass
        except Exception as e:
            log.warning('{}, waiting 15s...'.format(str(e).splitlines()[0]))
            time.sleep(15)
    log.info('Quitting...')


if __name__ == "__main__":
    online()
    stop.set()

